import smtplib

recipients = {}
MESSAGE = {}
mail_server = ''
port = ''


def run(conf, txt):
    global MESSAGE, recipients, mail_server, port
    MESSAGE['subject'] = txt['subject']
    MESSAGE['text'] = txt['text']

    if ('from' in conf) and conf['from']:
        from_addr=conf['from']
    else:
        print 'Error: no from address'
        return

    if ('server' in conf) and conf['server']:
        mail_server=conf['server']
    else:
        print 'Error: no server address'
        return

    if ('port' in conf) and conf['port']:
        port=conf['port']
    else:
        print 'Error: no server port'
        return

    if ('username' in conf) and conf['username']:
	   username=conf['username']
    else:
        print 'Error: no username'
        return

    if ('password' in conf) and conf['password']:
	   password=conf['password']
    else:
        print 'Error: no password'
        return

    if 'to' in conf:
        to_addr = conf['to']
        to_addr = to_addr.replace(' ', '')
        to_addr = to_addr.replace('\n', '')
        to_addr = to_addr.split(',')
        recipients['to'] = filter(None, to_addr)
    else:
        print 'Error: no to address'
        return
 
    if not recipients['to']:
        print 'Error: no to address'
        return
 
    if 'cc' in conf:
        cc_addr = conf['cc']
        cc_addr = cc_addr.replace(' ', '')
        cc_addr = cc_addr.replace('\n', '')
        cc_addr = cc_addr.split(',')
        recipients['cc'] = filter(None, cc_addr)

    if 'bcc' in conf:
        bcc_addr = conf['bcc']
        bcc_addr = bcc_addr.replace(' ', '')
        bcc_addr = bcc_addr.replace('\n', '')
        bcc_addr = bcc_addr.split(',')
        recipients['bcc'] = filter(None, bcc_addr)

    send_snmp_email(from_addr, username, password)

def send_snmp_email(fromaddr, username, password):
    message = 'From: %s\r\n' % fromaddr + 'To: %s\r\n' % ",".join(recipients['to']) + 'CC: %s\r\n' % ",".join(recipients['cc']) + 'Subject: %s\r\n' % MESSAGE['subject'] + '\r\n' + MESSAGE['text']
    
    print message
    toaddrs = recipients['to'] + recipients['cc'] + recipients['bcc']
    server = smtplib.SMTP_SSL(mail_server, port)
    server.ehlo()
    server.login(username, password)
    server.sendmail(fromaddr, toaddrs, message)
    server.quit()
