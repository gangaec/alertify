import tweepy

def get_api(cfg):
  auth = tweepy.OAuthHandler(cfg['consumer_key'], cfg['consumer_secret'])
  auth.set_access_token(cfg['access_token'], cfg['access_token_secret'])
  return tweepy.API(auth)

def run(conf, txt):
   api = get_api(conf)
   tweet = txt['text']
   status = api.update_status(status=tweet)
