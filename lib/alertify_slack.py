import requests

def run(conf, txt):
    api = conf['slack_webhook_url']
    data = '{ "text" : "' + str(txt['text']) + '"}'

    r = requests.post(api,data = data)
    print(r.status_code,r.reason)
