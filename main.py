#!/usr/bin/python

from ConfigParser import SafeConfigParser
import json
import logging
import sys

logger = logging.getLogger()

# format='%(asctime)s - %(levelname)s - %(message)s')
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s - %(message)s')

# if no methods specified, default method to send
DEFAULT_METHOD = 'email'
CONFIG_FILE = 'alertify.conf'
MODULE_PATH = 'lib/'
sys.path.insert(0, MODULE_PATH)
MODULE_STARTS_WITH = 'alertify_'

parser = SafeConfigParser()
parser.read(CONFIG_FILE)

CONF = {}


def get_default_methods():
    # quit if no default section found in CONFIG_FILE
    if not parser.has_section('default'):
        logging.error('No default section found')
        quit()

    # get the method
    if parser.has_option('default', 'methods'):
        methods = parser.get('default', 'methods')
        methods = methods.replace(' ', '')
        methods = methods.replace('\n', '')
    else:
        # no methods found, set default method
        methods = DEFAULT_METHOD

    if methods is '':
        logging.error('Error: No methods specified')
        quit()

    methods = methods.split(',')
    methods = filter(None, methods)
    logging.debug('Enabled methods: ' + str(methods))
    return methods


# check all sections available
def check_all_method(methods):
    for section in methods:
        if not parser.has_section(section):
            logging.debug('Section is not available for ' + section)
            return False
        logging.debug('Section is available for ' + section)
    return True


def get_conf_details():
    conf = {}
    conf['default'] = dict(parser.items('default'))
    conf['default'].pop('methods', None)
    for section in methods:
        conf[section] = dict(parser.items(section))
    return conf


def start_alerts(conf):
    for method in methods:
        m = __import__(MODULE_STARTS_WITH + method)
        init_conf = getattr(m, 'run')
        init_conf(conf[method], conf['default'])

methods = get_default_methods()
if check_all_method(methods) is False:
    logging.error('Quiting app. All sections not found')
    quit()
myconf = get_conf_details()
print myconf
start_alerts(myconf)
